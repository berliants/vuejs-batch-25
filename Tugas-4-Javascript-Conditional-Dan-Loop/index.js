// soal 1
var nilai = 100;
if (nilai >= 85) {
  console.log("nilai kamu A")
}
else if (nilai >= 75 && nilai < 85) {
  console.log("nilai kamu B")
}
else if (nilai >= 65 && nilai < 75) {
  console.log("nilai kamu C")
}
else if (nilai >= 55 && nilai < 65) {
  console.log("nilai kamu D")
}
else if (nilai < 55) {
  console.log("nilai kamu E")
}
else {
  console.log("harap masukkan nilai 1 - 100!")
}


// soal 2
var tanggal = 15;
var bulan = 5;
var tahun = 2001;
switch(bulan) {
  case 1 :
    console.log("15 Januari 2001")
    break
  case 2 :
    console.log("15 Februari 2001")  
    break
  case 3 :
    console.log("15 Maret 2001")  
    break 
  case 4 :
    console.log("15 April 2001")  
    break
  case 5 :
    console.log("15 Mei 2001")  
    break  
}


// soal 3
// n = 3
function segitiga (n) {
  var output = ""
  for (var i=1; i<=n; i++) {
    for (var j=1; j<=i; j++) {
      output += "#"
    } 
    output += "\n"
  }
  return output
}
console.log(segitiga(3))

// n = 7
console.log(segitiga(7))


// soal 4
var kata = [" - I love ", "programming", "Javascript", "VueJS"]

function m (n) {
  switch (n) {
    case 3 :
      for (var i=0; i<n; i++) {
        console.log(i+1 + kata[0] + kata[i+1]) 
      }
      console.log("===")
      break

    case 5 :
      for (var i=0; i<n-2; i++) {
        console.log(i+1 + kata[0] + kata[i+1]) 
      }
      console.log("===")
      for (var i=0; i<n-3; i++) {
        console.log(i+4 + kata[0] + kata[i+1]) 
      }
      break  

    case 7 :
      for (var i=0; i<n-4; i++) {
        console.log(i+1 + kata[0] + kata[i+1]) 
      }
      console.log("===")
      for (var i=0; i<n-4; i++) {
        console.log(i+4 + kata[0] + kata[i+1]) 
      }
      console.log("======")
      for (var i=0; i<n-6; i++) {
        console.log(i+7 + kata[0] + kata[i+1]) 
      }
      break

  case 10 :
    for (var i=0; i<n-7; i++) {
      console.log(i+1 + kata[0] + kata[i+1]) 
    }
    console.log("===")
    for (var i=0; i<n-7; i++) {
      console.log(i+4 + kata[0] + kata[i+1]) 
    }
    console.log("======")
    for (var i=0; i<n-7; i++) {
      console.log(i+7 + kata[0] + kata[i+1]) 
    }
    console.log("=========")
    for (var i=0; i<n-9; i++) {
      console.log(i+10 + kata[0] + kata[i+1]) 
    }   
    break
  }
}

m(10)
